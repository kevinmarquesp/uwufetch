extern crate get_shell;

use std::env;
use sys_info::{LinuxOSReleaseInfo, linux_os_release, disk_info};


const UWU: &str = r"
⡆⣐⢕⢕⢕⢕⢕⢕⢕⢕⠅⢗⢕⢕⢕⢕⢕⢕⢕⠕⠕⢕⢕⢕⢕⢕⢕⢕⢕⢕
⢐⢕⢕⢕⢕⢕⣕⢕⢕⠕⠁⢕⢕⢕⢕⢕⢕⢕⢕⠅⡄⢕⢕⢕⢕⢕⢕⢕⢕⢕
⢕⢕⢕⢕⢕⠅⢗⢕⠕⣠⠄⣗⢕⢕⠕⢕⢕⢕⠕⢠⣿⠐⢕⢕⢕⠑⢕⢕⠵⢕
⢕⢕⢕⢕⠁⢜⠕⢁⣴⣿⡇⢓⢕⢵⢐⢕⢕⠕⢁⣾⢿⣧⠑⢕⢕⠄⢑⢕⠅⢕
⢕⢕⠵⢁⠔⢁⣤⣤⣶⣶⣶⡐⣕⢽⠐⢕⠕⣡⣾⣶⣶⣶⣤⡁⢓⢕⠄⢑⢅⢑    $username$@$hostname$
⠍⣧⠄⣶⣾⣿⣿⣿⣿⣿⣿⣷⣔⢕⢄⢡⣾⣿⣿⣿⣿⣿⣿⣿⣦⡑⢕⢤⠱⢐
⢠⢕⠅⣾⣿⠋⢿⣿⣿⣿⠉⣿⣿⣷⣦⣶⣽⣿⣿⠈⣿⣿⣿⣿⠏⢹⣷⣷⡅⢐    os ~ $distro$
⣔⢕⢥⢻⣿⡀⠈⠛⠛⠁⢠⣿⣿⣿⣿⣿⣿⣿⣿⡀⠈⠛⠛⠁ ⣼⣿⣿⡇⢔    shell ~ $shell$
⢕⢕⢽⢸⢟⢟⢖⢖⢤⣶⡟⢻⣿⡿⠻⣿⣿⡟⢀⣿⣦⢤⢤⢔⢞⢿⢿⣿⠁⢕    disk ~ $disk$ GB
⢕⢕⠅⣐⢕⢕⢕⢕⢕⣿⣿⡄⠛⢀⣦⠈⠛⢁⣼⣿⢗⢕⢕⢕⢕⢕⢕⡏⣘⢕
⢕⢕⠅⢓⣕⣕⣕⣕⣵⣿⣿⣿⣾⣿⣿⣿⣿⣿⣿⣿⣷⣕⢕⢕⢕⢕⡵⢀⢕⢕    $RED$●  $GREEN$●  $BLUE$●  $YELLOW$●$RESET$
⢑⢕⠃⡈⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢃⢕⢕⢕
⣆⢕⠄⢱⣄⠛⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⢁⢕⢕⠕⢁
⣿⣦⡀⣿⣿⣷⣶⣬⣍⣛⣛⣛⡛⠿⠿⠿⠛⠛⢛⣛⣉⣭⣤⣂⢜⠕⢑⣡⣴⣿";


fn main() {
    let os: LinuxOSReleaseInfo = linux_os_release().unwrap_or_default();

    // Get the system info

    let username: String = env::var_os("USER")
        .unwrap().into_string().unwrap();

    let hostname: String = os.name().to_string()
        .to_lowercase().replace(" ", "");

    let distro: String = os.name().to_string();

    let shell: String = get_shell::get_shell_name().unwrap();

    let disk: u64 =  disk_info().unwrap().total / 1024000;

    // Write the UwU face on screen

    let final_text = UWU
        .replace("$username$", username.as_str())
        .replace("$hostname$", hostname.as_str())
        .replace("$distro$",   distro.as_str())
        .replace("$shell$",    shell.as_str())
        .replace("$disk$",     disk.to_string().as_str())
        .replace("$RED$",      "\x1b[31m")
        .replace("$GREEN$",    "\x1b[32m")
        .replace("$BLUE$",     "\x1b[34m")
        .replace("$YELLOW$",   "\x1b[33m")
        .replace("$RESET$",    "\x1b[0m");

    println!("{}", final_text);
}
