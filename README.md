# UWUfetch &#x2014; *Fetch CLI application written in Rust*

This application was written using the "Quick and dirty" method (I'm sorry...) to train a little bit of Rust programming language and gain experience by that. Fell free to contribute/give some advice/make fun of me. **UwU**


## Instalation

```shell
git clone https://codeberg.org/kevinmarquesp/uwufetch
cd uwufetch
cargo build --release
```


## Usage

Just run `uwufetch` and see what's gonna happen.


## Development

### Todos

+ [ ] Use an template to read the ASCII code output (like pandoc)
+ [ ] Use a `Makefile` to create the config file and store the template
+ [ ] Test it on different operating systems
